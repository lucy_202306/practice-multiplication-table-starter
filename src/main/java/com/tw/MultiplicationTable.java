package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        return generateTable(start, end);
    }

    public Boolean isValid(int start, int end) {

        return isInRange(start) && isInRange(end) && isStartNotBiggerThanEnd(start, end);
    }

    public Boolean isInRange(int number) {

        return number >= 1 && number <=1000;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {

        return start <= end;
    }

    public String generateTable(int start, int end) {
        String table = "";
        for (int i = start; i < end; i++) {
            table += generateLine(start, i) + "\n";
        }
        table += generateLine(start, end);
        return table;
    }

    public String generateLine(int start, int row) {
        String expected = "";
        for (int i = start; i <= row; i++) {
            expected += generateSingleExpression(i, row) + "  ";
        }

        return expected.trim();
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {

        int result = multiplicand * multiplier;

        return multiplicand + "*" + multiplier + "=" + result;
    }
}
